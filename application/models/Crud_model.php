<?php
class Crud_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    // Cek id
    public function cek_id($table, $field)
    {
        $this->db->select('*');
        $this->db->order_by($field, "DESC");
        $this->db->limit(1);
        $cek = $this->db->get($table)->row();
        if (empty($cek)) {
            return "1";
        } else {
            $data = $cek->$field + 1;
            return $data;
        }
    }

    // cek data kalau sudah ada atau belum
    public function cek_data($table, $field, $id)
    {
        $this->db->select('*');
        $this->db->where($field, $id);
        $cek = $this->db->get($table)->row();
        if (empty($cek)) {
            return true;
        } else {
            return false;
        }
    }

    // cek data kalau sudah ada atau belum dengan where array
    public function cek_data_where_array($table, $where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $cek = $this->db->get($table)->row();
        if (empty($cek)) {
            return true;
        } else {
            return false;
        }
    }

    // cek data kalau sudah ada atau belum
    public function cek_data_ex($table, $field, $id, $ex_field, $ex_id)
    {
        $this->db->select('*');
        $this->db->where($field, $id);
        $this->db->where($ex_field, $ex_id);
        $cek = $this->db->get($table)->row();
        if (empty($cek)) {
            return true;
        } else {
            return false;
        }
    }

    // ======== ********************************************************************** ==============

    // select
    public function select_all($table)
    {
        $this->db->select('*');
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where order
    public function select_all_where_order($table, $where, $whereby, $order, $order_by)
    {
        $this->db->select('*');
        $this->db->where($where, $whereby);
        $this->db->order_by($order, $order_by);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select order
    public function select_all_order($table, $field, $by)
    {
        $this->db->select('*');
        $this->db->order_by($field, $by);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where
    public function select_all_where($table, $field, $key)
    {
        $this->db->select('*');
        $this->db->where($field, $key);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where array
    public function select_all_where_array($table, $where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where array row
    public function select_all_where_array_row($table, $where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where group
    public function select_all_where_group($table, $field, $key, $group)
    {
        $this->db->select('*');
        $this->db->where($field, $key);
        $this->db->group_by($group);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where limit
    public function select_all_where_limit($table, $field, $key, $order, $order_by, $limit)
    {
        $this->db->select('*');
        $this->db->where($field, $key);
        $this->db->order_by($order, $order_by);
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where array limit
    public function select_all_where_array_limit($table, $field, $key, $order, $orderby, $limit)
    {
        $this->db->select('*');
        $this->db->where($field, $key);
        $this->db->order_by($order, $orderby);
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select where array order
    public function select_all_where_array_order($table, $where, $order, $orderby)
    {
        $this->db->select('*');
        $this->db->where($where);
        $this->db->order_by($order, $orderby);
        $query = $this->db->get($table);
        return $query->result();
    }

    public function select_one($table, $field, $key)
    {
        $this->db->select('*');
        $this->db->where($field, $key);
        $query = $this->db->get($table);
        return $query->row();
    }

    // select row array *** untuk update userdata user
    public function select_one_row_array($table, $field, $key)
    {
        $this->db->select('*');
        $this->db->where($field, $key);
        $query = $this->db->get($table);
        return $query->row_array();
    }

    public function select_one_where_array($table, $where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->row();
    }

    // select  kembalian jumlah data
    public function select_all_num_row($table)
    {
        $this->db->select('*');
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    // select where kembalian jumlah data
    public function select_all_where_num_row($table, $field, $key)
    {
        $this->db->select('*');
        $this->db->where($field, $key);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    // select where array
    public function select_all_where_array_num_row($table, $where)
    {
        $this->db->select('*');
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    // select where array + like
    public function select_all_where_array_like_num_row($table, $where, $like = null, $like_match = null)
    {
        $this->db->select('*');
        $this->db->where($where);
        if ($like != null) {
            $this->db->like($like, $like_match);
        }
        $query = $this->db->get($table);
        return $query->num_rows();
    }

    // select max
    public function select_max($table, $field, $where, $key)
    {
        $this->db->select_max($field);
        $this->db->where($where, $key);
        $cek = $this->db->get($table)->row();
        if ($cek == null) {
            return "1";
        } else {
            $data = $cek->$field + 1;
            return $data;
        }
    }

    // select beberapa field semua
    public function select_by_field($table, $field, $where)
    {
        $this->db->select($field);
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();
    }

    // select beberapa field baris
    public function select_by_field_row($table, $field, $where)
    {
        $this->db->select($field);
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->row();
    }

    // Select paginasi
    public function select_paging($table, $number, $offset)
    {
        return $query = $this->db->get($table, $number, $offset)->result();
    }

    // Select paginasi where array
    public function select_paging_where_array($table, $where, $number, $offset)
    {
        return $query = $this->db->get($table, $number, $offset)->result();
    }

    // limit for paginasi
    public function select_paging_where($table, $where, $limit, $start, $order, $order_by)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by($order, $order_by);
        $this->db->where($where);
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    // limit for paginasi
    public function select_paging_where_like($table, $where, $limit, $start, $order, $order_by, $like = null, $like_match = null)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by($order, $order_by);
        $this->db->where($where);
        if ($like != null) {
            $this->db->like($like, $like_match);
        }
        $query = $this->db->get($table);

        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    // select sum
    public function select_sum($table, $field, $where)
    {
        $this->db->select_sum($field);
        $this->db->where($where);
        $query = $this->db->get($table);
        $data = $query->row();
        return $data->$field;
    }

    public function select_custom($query)
    {
        $q = $this->db->query($query);
        return $q->result();
    }

    public function select_custom_row($query)
    {
        $q = $this->db->query($query);
        return $q->row();
    }

    public function custom_query($query)
    {
        $q = $this->db->query($query);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function insert($table, $data)
    {
        $q = $this->db->insert($table, $data);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function insert_batch($table, $data)
    {
        $q = $this->db->insert_batch($table, $data);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function update($table, $data, $key, $id)
    {
        $this->db->where($key, $id);
        $q = $this->db->update($table, $data);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    public function update_where_array($table, $data, $where)
    {
        $this->db->where($where);
        $q = $this->db->update($table, $data);
        if ($q) {
            return true;
        } else {
            return false;
        }
    }

    // hapus id
    public function hapus_id($table, $field, $id)
    {
        $this->db->where($field, $id);
        $del = $this->db->delete($table);
        if ($del) {
            return true;
        } else {
            return false;
        }
    }
}
