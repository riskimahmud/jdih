<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class M_berita extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
        if (empty($this->session->userdata('user'))) {
            redirect('Login/logout');
        }
    }

    public $tabel = 'jdih_web';
    public $label = 'Berita';
    public $base = 'm_berita';
    public $page = '/berita';
    public $key = 'id';
    public $ket = array();
    public $bread = array();
    public $kategori_arr = ["profil", "berita", "pengumuman"];

    public function index($kategori)
    {
        if (in_array($kategori, $this->kategori_arr)) {
            $a = array();

            $a['page'] = $this->page;
            $a['title'] = strtoupper($kategori);
            // $a['title']        =    "";
            $a['base'] = $kategori;
            $a['ket'] = $this->ket;
            $a['data'] = $this->crud_model->select_all_where_order($this->tabel, "kategori", $kategori, $this->key, "DESC");

            $this->bread[] = array(
                "active" => false,
                "icon" => "icon-home home-icon",
                "link" => site_url(),
                "label" => "Dashboard",
                "divider" => true,
            );
            $this->bread[] = array(
                "active" => true,
                "icon" => "",
                "link" => "",
                "label" => strtoupper($kategori),
                "divider" => false,
            );

            $a['bread'] = $this->bread;
            $a['tabel'] = $this->tabel;
            $a['key'] = $this->key;
            $a['label'] = "DAFTAR " . strtoupper($kategori);
            $a['kategori'] = $kategori;
            if ($kategori != "profil") {
                $a['tombol_head'] = true;
            } else {
                // $a['tombol_head'] = false;
            }

            $this->load->library('ckeditor');
            $this->load->library('ckfinder');

            $this->ckeditor->basePath = base_url() . 'assets/ckeditor/';
            $this->ckeditor->config['toolbar'] = "FULL";
            $this->ckeditor->config['language'] = 'in';
            $this->ckeditor->config['width'] = '730px';
            $this->ckeditor->config['height'] = '300px';

            //Add Ckfinder to Ckeditor
            $this->ckfinder->SetupCKEditor($this->ckeditor, base_url() . 'assets/ckfinder/');
            $this->load->view("backend/main", $a);
        } else {
            redirect("admin/home");
        }
    }

    // tambah aksi
    public function tambah_aksi()
    {
        $id = $this->crud_model->cek_id($this->tabel, $this->key);
        $judul = $this->input->post("judul");
        $isi = $this->input->post("isi");
        $data = array(
            "id" => $id,
            "kategori" => $this->input->post('kategori', true),
            "judul" => $judul,
            "judul_seo" => url_title($judul, "-", true),
            "isi" => $isi,
            "hits" => 0,
        );

        $config['upload_path'] = './uploads/berita/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 2000;
        $config['max_filename'] = '255';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            $data_file = $this->upload->data();
            $data['gambar'] = $data_file['file_name'];
        }

        $tambah = $this->crud_model->insert($this->tabel, $data);
        if ($tambah) {
            $notifikasi = array(
                "status" => 1, "pesan" => strtoupper($this->input->post('kategori', true)) . " Berhasil Ditambah",
            );
        } else {
            $notifikasi = array(
                "status" => 0, "pesan" => strtoupper($this->input->post('kategori', true)) . " Gagal Ditambah",
            );
        }
        $this->session->set_flashdata("notifikasi", $notifikasi);
        redirect($this->input->post('kategori', true));
    }

    // perbarui aksi
    public function perbarui_aksi()
    {
        $id = $this->input->post("id");
        $judul = $this->input->post("judul");
        $isi = $this->input->post("isi");
        $status = $this->input->post("status");
        $data = array(
            "judul" => $judul,
            "judul_seo" => url_title($judul, "-", true),
            "isi" => $isi,
            "status" => $status,
        );

        $detail = $this->crud_model->select_one($this->tabel, $this->key, $id);

        $config['upload_path'] = './uploads/berita/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size'] = 2000;
        $config['max_filename'] = '255';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        if ($this->upload->do_upload('gambar')) {
            if ($detail->gambar != "") {
                unlink('./uploads/berita/' . $detail->gambar);
            }
            $data_file = $this->upload->data();
            $data['gambar'] = $data_file['file_name'];
        }

        $perbarui = $this->crud_model->update($this->tabel, $data, $this->key, $id);
        if ($perbarui) {
            $notifikasi = array(
                "status" => 1, "pesan" => strtoupper($detail->kategori) . " Berhasil Diperbarui",
            );
        } else {
            $notifikasi = array(
                "status" => 0, "pesan" => strtoupper($detail->kategori) . " Gagal Diperbarui",
            );
        }
        $this->session->set_flashdata("notifikasi", $notifikasi);
        redirect($this->input->post('kategori', true));
    }

    // Fungsi hapus
    public function hapus($id)
    {
        $data = $this->crud_model->select_one($this->tabel, $this->key, $id);
        $hapus = $this->crud_model->hapus_id($this->tabel, $this->key, $id);
        if ($hapus) {
            $notifikasi = array(
                "status" => 1, "pesan" => strtoupper($data->kategori) . " Berhasil Dihapus",
            );
            unlink('./uploads/berita/' . $data->gambar);
        } else {
            $notifikasi = array(
                "status" => 0, "pesan" => strtoupper($data->kategori) . " Gagal Dihapus",
            );
        }
        $this->session->set_flashdata("notifikasi", $notifikasi);
        redirect($this->base);
    }
}
