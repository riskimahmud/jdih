<?php
	class Laporan extends CI_Controller {
		
		public $direktori	=	"/laporan/";
		
		public function index()
		{
			redirect("e404");
		}
		
		public function ajax()
		{
			$fungsi	=	$this->input->post("fungsi");
			if($fungsi	==	"ambil_kelurahan"){
				$id_kecamatan	=	$this->input->post("kecamatan");
				$a['data']		=	$this->crud_model->select_all_where("m_kelurahan","id_kecamatan",$id_kecamatan);
				$this->load->view("backend/laporan/ambil_kelurahan",$a);
			}elseif($fungsi	==	"cek_capen"){
				$this->load->model("join_model");
				$jenis			=	$this->input->post("jenis");
				$id_kecamatan	=	$this->input->post("kecamatan");
				$id_kelurahan	=	$this->input->post("kelurahan");
				$where			=	array(
					$jenis.".status"	=>	"a"
				);
				if($id_kecamatan != "0"){
					$where['m_kelurahan.id_kecamatan']	=	$id_kecamatan;
				}
				if($id_kelurahan != "0"){
					$where[$jenis.'.id_kelurahan']	=	$id_kelurahan;
				}
				
				if($jenis	==	"capen_lansia"){ $calon_penerima		=	"Calon Penerima Lansia Produktif"; }
				if($jenis	==	"capen_disabilitas"){ $calon_penerima	=	"Calon Penerima Bantuan Disabilitas"; }
				if($jenis	==	"capen_balitar"){ $calon_penerima		=	"Calon Penerima Bantuan Bayi Terlantar"; }
				if($jenis	==	"capen_bpnt"){ $calon_penerima			=	"Calon Penerima Bantuan Pangan Non Tunai"; }
				
				$a['jenis']		=	$calon_penerima;
				$a['kategori']	=	$jenis;
				$a['data']		=	$this->join_model->dua_tabel_where_array($jenis,"m_kelurahan","id_kelurahan",$where);
				$this->load->view("backend/laporan/ambil_capen",$a);
			}elseif($fungsi	==	"cek_bantuan"){
				$jenis			=	$this->input->post("jenis");
				$id_kecamatan	=	$this->input->post("kecamatan");
				$where			=	array();
				if($id_kecamatan != "0"){
					$where['id_kecamatan']	=	$id_kecamatan;
				}
				if($jenis != "0"){
					$where['jenis_bantuan']	=	$jenis;
				}
				$a['data']		=	$this->crud_model->select_all_where_array("bantuan",$where);
				$this->load->view("backend/laporan/ambil_bantuan",$a);
			}
		}
		
		public function capen()
		{
			$a['page']		=	"capen";
			$a['direktori']	=	$this->direktori;
			$a['title']		=	"Laporan Calon Penerima";
			$a['jenis']		=	$this->crud_model->select_all("jenis_bantuan");
			$a['kecamatan']	=	$this->crud_model->select_all_order("m_kecamatan","nama_kecamatan","ASC");
			$this->load->view("backend/main",$a);
		}
		
		public function cetak_capen($capen,$id){
			$penerima		=	$this->crud_model->select_one($capen,"nik",$id);
			$a['kecamatan']	=	ambil_nama_by_id("m_kelurahan","id_kecamatan","id_kelurahan",$penerima->id_kelurahan);
			$a['penerima']	=	$penerima;
			$a['capen']		=	$capen;
			$a['kepala']	=	$this->crud_model->select_one("config","id","1");
			$a['bantuan']	=	$this->crud_model->select_all_where_order("penerima_bantuan","id_penerima",$id,"id_bantuan","DESC");
			$this->load->view("backend/laporan/cetak_penerima",$a);
		}
		
		public function bantuan()
		{
			$a['page']		=	"bantuan";
			$a['direktori']	=	$this->direktori;
			$a['title']		=	"Laporan Bantuan";
			$a['jenis']		=	$this->crud_model->select_all("jenis_bantuan");
			$a['kecamatan']	=	$this->crud_model->select_all_order("m_kecamatan","nama_kecamatan","ASC");
			$this->load->view("backend/main",$a);
		}
		
		public function user()
		{
			$a['page']		=	"user";
			$a['direktori']	=	$this->direktori;
			$a['title']		=	"User";
			$a['judul']		=	"User";
			$a['data']		=	$this->crud_model->select_all_order("users","level","DESC");
			$this->load->view("backend/main",$a);
		}
		
		public function informasi()
		{
			$a['page']		=	"informasi";
			$a['direktori']	=	$this->direktori;
			$a['title']		=	"Informasi";
			$a['judu']		=	"Informasi";
			$a['data']		=	$this->crud_model->select_all_order("informasi","create_at","DESC");
			$this->load->view("backend/main",$a);
		}
	}
?>