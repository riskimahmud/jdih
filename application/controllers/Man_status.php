<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Man_status extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		if(empty($this->session->userdata('user'))){
			redirect('Login/logout');
		}
	}
	
	public $tabel	='man_status';
	public $label	='Manajemen Status';
	public $base	='man_status';
	public $page	='/man_status';
	public $key		='';
	public $ket		= array("0" => "id_man", "1" => "id_kategori", "2" => "id_status");
	public $bread	= array();
	
	public function index()
	{
			$a	=	array();
			
			$a['page']		=	$this->page;
			$a['title']		=	$this->label;
			// $a['title']		=	"";
			$a['base']		=	$this->base;
			$a['ket']		=	$this->ket;
			// $a['data']		=	$this->crud_model->select_all_order($this->tabel,"id_status","ASC");
			$a['data']		=	$this->crud_model->select_all_order("kategori","id_kategori","ASC");
			
			$this->bread[]	=	array(
				"active"	=>	FALSE,
				"icon"		=>	"icon-home home-icon",
				"link"		=>	site_url(),
				"label"		=>	"Dashboard",
				"divider"	=>	TRUE,
			);
			
			$this->bread[]	=	array(
				"active"	=>	TRUE,
				"icon"		=>	"",
				"link"		=>	"",
				"label"		=>	$this->label,
				"divider"	=>	FALSE,
			);
			
			$a['bread']		=	$this->bread;
			$a['tabel']		=	$this->tabel;
			$a['key']		=	$this->key;
			$a['label']		=	$this->label;
			// $a['tombol_head']	=	TRUE;
			$this->load->view("backend/main", $a);
	}
	
	// perbarui aksi
	public function perbarui_aksi(){
		$id			=	$this->input->post("id");
		$nama		=	ucwords($this->input->post("nama"));
		$status		=	$this->input->post("status");
		$data		=	array();
		$this->crud_model->hapus_id("man_status","id_kategori",$id);
		foreach($status as $key => $value){
			$data[$key]['id_kategori']			=	$id;
			$data[$key]['id_status']			=	$value;
		}
		$perbarui	=	$this->crud_model->insert_batch($this->tabel,$data);
		if($perbarui){
			$notifikasi		=	array(
				"status"	=>	1, "pesan"	=>	$nama." Berhasil Diperbarui"
			);
		}else{
			$notifikasi		=	array(
				"status"	=>	0, "pesan"	=>	$nama." Gagal Diperbarui"
			);
		}
		$this->session->set_flashdata("notifikasi",$notifikasi);
		redirect($this->base);
	}
	
}