<?php
class Frontend extends CI_Controller
{
    // function __construct()
    // {
    // parent::__construct();
    //date_default_timezone_set('Asia/Singapore');
    //$this->load->model('users_model');
    // }

    public $base = 'frontend';
    public $konten = '/frontend';

    public function index()
    {
        $data['title'] = "Dashboard";
        $data['page'] = "/dashboard";
        $data['kategori'] = $this->crud_model->select_all_order("kategori", "id_kategori", "ASC");
        $data['phukum'] = $this->crud_model->select_all_where_limit("draft", "status", "12", "id", "DESC", "5");
        $data['berita'] = $this->crud_model->select_all_where_limit("jdih_web", "kategori", "berita", "id", "DESC", "4");
        $this->load->view("frontend/main", $data);
    }

    public function produk()
    {
        $this->session->unset_userdata("key");
        $this->session->unset_userdata("like");
        $data['title'] = "Produk Hukum";
        $data['page'] = "/produk";
        $data['dashboard'] = false;
        $data['kat'] = "semua";

        $where = array();
        $key = array();
        $like = null;
        $like_match = null;

        $where['status'] = "12";
        $where['kategori <'] = "3";
        $jumlah_data = $this->crud_model->select_all_where_array_like_num_row("draft", $where, $like, $like_match);
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'produk/semua';
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 10;
        $from = $this->uri->segment(3);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        $this->pagination->initialize($config);
        $data['data'] = $this->crud_model->select_paging_where("draft", $where, $config['per_page'], $from, "id", "DESC");
        $data['kategori'] = $this->crud_model->select_all_where_order("kategori", "id_kategori <", "3", "id_kategori", "ASC");
        $data['status'] = $this->crud_model->select_all_order("keterangan", "id_keterangan", "ASC");

        $this->load->view("frontend/main", $data);
    }

    public function produkbyKategori($kategori)
    {
        $this->session->unset_userdata("key");
        $this->session->unset_userdata("like");
        $data['title'] = "PRODUK";
        $data['page'] = "/produk";
        $data['dashboard'] = false;

        $where = array();
        $key = array();
        $like = null;
        $like_match = null;

        $where['status'] = "12";
        // $where['kategori <'] = "3";
        $where['kategori'] = $kategori;

        $jumlah_data = $this->crud_model->select_all_where_array_like_num_row("draft", $where, $like, $like_match);
        $this->load->library('pagination');
        if ($kategori == 1) {
            $data['title'] = "PERATURAN DAERAH";
            $data['kategori'] = "1";
            $data['kat'] = "1";
            $config['base_url'] = base_url() . 'produk/peraturan-daerah';
        } else if ($kategori == 2) {
            $data['title'] = "PERATURAN WALIKOTA";
            $data['kategori'] = "2";
            $data['kat'] = "2";
            $config['base_url'] = base_url() . 'produk/peraturan-walikota';
        } else {
            $data['kat'] = "4";
            $data['kategori'] = "4";
            $data['title'] = "KEPUTUSAN SEKRETARIS DAERAH";
            $config['base_url'] = base_url() . 'produk/peraturan-lainnya';
        }

        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 10;
        $from = $this->uri->segment(3);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        $this->pagination->initialize($config);
        $data['data'] = $this->crud_model->select_paging_where("draft", $where, $config['per_page'], $from, "id", "DESC");
        $data['kategori'] = $this->crud_model->select_all_where_order("kategori", "id_kategori <", "3", "id_kategori", "ASC");
        $data['status'] = $this->crud_model->select_all_order("keterangan", "id_keterangan", "ASC");

        $this->load->view("frontend/main", $data);
    }

    public function filter()
    {
        $data['kat'] = "semua";
        if ($this->input->post("filter")) {

            $where = array();
            $key = array();
            $like = null;
            $like_match = null;
            // if ($this->input->post('kategori') != null) {
            $kategori = $this->input->post("kategori");
            $status = $this->input->post("status");
            $nomor = $this->input->post("nomor");
            $tahun = $this->input->post("tahun");
            $tentang = $this->input->post("tentang");
            $where['kategori <'] = "3";
            $key['kategori <'] = "3";
            if ($kategori != "" && $kategori != "0") {
                $where['kategori'] = $kategori;
                $key['kategori'] = $kategori;
            }
            if ($status != "" && $status != "0") {
                $where['ket'] = $status;
                $key['ket'] = $status;
            }
            if ($nomor != "") {
                $where['nomor'] = $nomor;
                $key['nomor'] = $nomor;
            }
            if ($tahun != "") {
                $where['tahun'] = $tahun;
                $key['tahun'] = $tahun;
            }
            if ($tentang != "") {
                $like = "tentang";
                $like_match = $tentang;
                // $key['tentang']     = $tentang;
                $this->session->set_userdata("like", $tentang);
            }
            $this->session->set_userdata(array("key" => $key));
            // print_r($this->session->userdata("key"));
            // } else {
        } else {
            $where = $this->session->userdata('key');
            $like = "tentang";
            $like_match = $this->session->userdata("like");
        }

        $data['title'] = "Produk Hukum";
        $data['page'] = "/produk";
        $data['dashboard'] = false;
        // $jumlah_data     =     $this->crud_model->select_all_where_array_num_row("draft",$where);
        $jumlah_data = $this->crud_model->select_all_where_array_like_num_row("draft", $where, $like, $like_match);
        $this->load->library('pagination');
        $from = $this->uri->segment(3);
        $config['base_url'] = base_url() . 'produk/filter/';
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 10;
        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        $this->pagination->initialize($config);
        $data['data'] = $this->crud_model->select_paging_where_like("draft", $where, $config['per_page'], $from, "tahun", "DESC", $like, $like_match);
        $data['kategori'] = $this->crud_model->select_all_where_order("kategori", "id_kategori <", "3", "id_kategori", "ASC");
        $data['status'] = $this->crud_model->select_all_order("keterangan", "id_keterangan", "ASC");

        // echo "<pre>";
        // print_r($where);
        // echo "</pre>";

        $this->load->view("frontend/main", $data);
    }

    public function detail_produk($id)
    {
        $detail = $this->crud_model->select_one("draft", "id", $id);
        $hits = $detail->hits + 1;
        $this->crud_model->update("draft", array("hits" => $hits), "id", $id);
        $data['data'] = $this->crud_model->select_one("draft", "id", $id);
        $data['title'] = "Detail Produk Hukum";
        $data['page'] = "/detail_produk";
        $data['dashboard'] = false;
        $this->load->view("frontend/main", $data);
    }

    public function download()
    {
        $id = $this->uri->segment(3);
        $detail = $this->crud_model->select_one("draft", "id", $id);
        $download = $detail->downloads + 1;
        $this->crud_model->update("draft", array("downloads" => $download), "id", $id);
        $this->load->helper('download');
        $nama = $detail->pdf;
        $data = file_get_contents("./uploads/pdf/" . $detail->pdf);
        force_download($nama, $data);
    }

    public function pengaduan()
    {
        $data['title'] = "Pengaduan";
        $data['page'] = "/pengaduan";
        $data['dashboard'] = false;
        $this->load->view("frontend/main", $data);
    }

    public function pengaduan_aksi()
    {
        $nama = $this->input->post("nama");
        $no_ktp = $this->input->post("no_ktp");
        $no_telp = $this->input->post("no_telp");
        $pekerjaan = $this->input->post("pekerjaan");
        $alamat = $this->input->post("alamat");
        $terlapor = $this->input->post("terlapor");
        $isi_aduan = $this->input->post("isi_aduan");
        $data = array(
            "id" => $this->crud_model->cek_id("aduan", "id"),
            "nama" => $nama,
            "no_ktp" => $no_ktp,
            "no_telp" => $no_telp,
            "pekerjaan" => $pekerjaan,
            "alamat" => $alamat,
            "terlapor" => $terlapor,
            "isi_aduan" => $isi_aduan,
            "tanggal_aduan" => date("Y-m-d"),
            "status" => "new",
        );
        $insert = $this->crud_model->insert("aduan", $data);
        if ($insert) {
            $notifikasi = array(
                "status" => 1, "pesan" => "Aduan Berhasil Dikirim",
            );
        } else {
            $notifikasi = array(
                "status" => 0, "pesan" => "Aduan Gagal Dikirim",
            );
        }
        // print_r($data);
        $this->session->set_flashdata("notifikasi", $notifikasi);
        redirect("frontend/pengaduan");
    }

    public function chat()
    {
        $nama = $this->input->post("name");
        $email = $this->input->post("email");
        $judul = $this->input->post("subject");
        $pesan = $this->input->post("message");
        $data = array(
            "nama" => $nama,
            "email" => $email,
            "judul" => $judul,
            "pesan" => $pesan,
            "status" => "0",
        );
        $simpan = $this->crud_model->insert("kontak", $data);
        if ($simpan) {
            echo "OK";
        } else {
            echo "Gagal";
        }
    }

    public function berita()
    {
        $data['title'] = "Berita";
        $data['page'] = "/berita";
        $data['dashboard'] = false;

        $where = array();
        $key = array();
        $where['kategori'] = "berita";
        $jumlah_data = $this->crud_model->select_all_where_array_num_row("jdih_web", $where);
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'frontend/berita/';
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 10;
        $from = $this->uri->segment(3);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['next_link'] = 'Next';
        $config['prev_link'] = 'Prev';
        $config['full_tag_open'] = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close'] = '</ul></nav></div>';
        $config['num_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close'] = '</span></li>';
        $config['cur_tag_open'] = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close'] = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close'] = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close'] = '</span>Next</li>';
        $config['first_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open'] = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close'] = '</span></li>';
        $this->pagination->initialize($config);
        $data['data'] = $this->crud_model->select_paging_where("jdih_web", $where, $config['per_page'], $from, "create_at", "DESC");
        $data['populer'] = $this->crud_model->select_paging_where("jdih_web", $where, $config['per_page'], $from, "hits", "DESC");

        $this->load->view("frontend/main", $data);
    }

    public function detail_berita($id)
    {
        $data['title'] = "Detail Berita";
        $data['page'] = "/detail_berita";
        $data['dashboard'] = false;
        $data['data'] = $this->crud_model->select_one("jdih_web", "id", $id);
        $where['kategori'] = "berita";
        $jumlah_data = $this->crud_model->select_all_where_array_num_row("jdih_web", $where);
        $this->load->library('pagination');
        $config['base_url'] = base_url() . 'frontend/berita/';
        $config['total_rows'] = $jumlah_data;
        $config['per_page'] = 10;
        $from = $this->uri->segment(3);

        $data['populer'] = $this->crud_model->select_paging_where("jdih_web", $where, $config['per_page'], $from, "hits", "DESC");

        $this->load->view("frontend/main", $data);
    }

    public function kontak()
    {
        $data['title'] = "Kontak";
        $data['page'] = "/kontak";

        $this->load->view("frontend/main", $data);
    }

    public function profil($kategori = null)
    {

        $data['page'] = "/profil";
        if ($kategori == "visimisi") {
            $data['title'] = "VISI DAN MISI";
        } else if ($kategori == "struktur-organisasi") {
            $data['title'] = "STRUKTUR ORGANISASI";
        }
        $where['kategori'] = $kategori;
        $data['profil'] = $this->crud_model->select_all_where_array("jdih_web", $where);

        $this->load->view("frontend/main", $data);
    }
}
