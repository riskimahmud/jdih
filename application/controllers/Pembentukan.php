<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pembentukan extends CI_Controller {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		if(empty($this->session->userdata('user'))){
			redirect('Login/logout');
		}
	}
	
	public $tabel	='draft';
	public $label	='Pembentukan';
	public $base	='pembentukan';
	public $page	='/pembentukan';
	public $key		='id';
	public $ket		= array();
	public $bread	= array();
	
	public function index()
	{
			$a	=	array();
			
			$a['page']		=	$this->page;
			$a['title']		=	$this->label;
			$a['base']		=	$this->base;
			$a['ket']		=	$this->ket;
			$where			=	array();
			$key			=	array();
			$like			=	NULL;
			$like_match		=	NULL;
			if($this->session->userdata("user")['type'] == "skpd"){
				$where['skpd']	=	$this->session->userdata("user")['nama'];
			}
			// $jumlah_data 	= 	$this->crud_model->select_all_num_row("draft");
			$jumlah_data 	= 	$this->crud_model->select_all_where_array_like_num_row("draft",$where,$like,$like_match);
			$this->load->library('pagination');
			$config['base_url'] = base_url().'pembentukan/index/';
			$config['total_rows'] = $jumlah_data;
			$config['per_page'] = 10;
			$from = $this->uri->segment(3);
 
			// Membuat Style pagination untuk BootStrap v4
			$config['first_link']       = 'First';
			$config['last_link']        = 'Last';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
			$config['full_tag_close']   = '</ul></nav></div>';
			$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
			$config['num_tag_close']    = '</span></li>';
			$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close']  = '</span>Next</li>';
			$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close']  = '</span></li>';
			$this->pagination->initialize($config);		
			$a['data']					=	$this->crud_model->select_paging_where($this->tabel,$where,$config['per_page'],$from,"id","DESC");
			$a['kategori']				=	$this->crud_model->select_all_order("kategori","id_kategori","ASC");
			$a['status']				=	$this->crud_model->select_all_order("status","id_status","ASC");
			
			$this->bread[]	=	array(
				"active"	=>	FALSE,
				"icon"		=>	"icon-home home-icon",
				"link"		=>	site_url(),
				"label"		=>	"Dashboard",
				"divider"	=>	TRUE,
			);
			
			$this->bread[]	=	array(
				"active"	=>	TRUE,
				"icon"		=>	"",
				"link"		=>	"",
				"label"		=>	"Pembentukan",
				"divider"	=>	FALSE,
			);
			
			$a['bread']		=	$this->bread;
			$a['tabel']		=	$this->tabel;
			$a['key']		=	$this->key;
			$a['aksi']		=	"";
			$this->load->view("backend/main", $a);
	}
	
	public function filter()
	{
			$a	=	array();
			$where			=	array();
			$key			=	array();
			$like			=	NULL;
			$like_match		=	NULL;
			if($this->input->post('kategori') != NULL ){
				$kategori		=	$this->input->post("kategori");
				$status			=	$this->input->post("status");
				$nomor			=	$this->input->post("nomor");
				$tahun			=	$this->input->post("tahun");
				$tentang		=	$this->input->post("tentang");
				if($kategori != "" && $kategori != "0"){ 
					$where['kategori'] 	= $kategori; 
					$key['kategori'] 	= $kategori; 
				}					
				if($status != "" && $status != "0"){ 
					$where['status'] 	= $status; 
					$key['status'] 		= $status; 
				}					
				if($nomor 	 != ""){ 
					$where['nomor'] 	= $nomor; 
					$key['nomor'] 		= $nomor; 
				}
				if($tahun 	 != ""){ 
					$where['tahun'] 	= $tahun; 
					$key['tahun'] 		= $tahun; 
				}
				if($tentang	 != ""){ 
					$like		 		= "tentang"; 
					$like_match		 	= $tentang; 
					// $key['tentang'] 	= $tentang; 
				}
				$this->session->set_userdata(array("key"=>$key));
			}else{
			  if($this->session->userdata('key') != NULL){
				$where = $this->session->userdata('key');
			  }
			}
			
			$a['page']		=	$this->page;
			$a['title']		=	$this->label;
			$a['base']		=	$this->base;
			$a['ket']		=	$this->ket;
			if($this->session->userdata("user")['type'] == "skpd"){
				$where['skpd']	=	$this->session->userdata("user")['nama'];
			}
			// $jumlah_data 	= 	$this->crud_model->select_all_where_array_num_row("draft",$where);
			$jumlah_data 	= 	$this->crud_model->select_all_where_array_like_num_row("draft",$where,$like,$like_match);
			$this->load->library('pagination');
			$from = $this->uri->segment(3);
			$config['base_url'] = base_url().'pembentukan/filter/';
			$config['total_rows'] = $jumlah_data;
			$config['per_page'] = 10;
			// Membuat Style pagination untuk BootStrap v4
			$config['first_link']       = 'First';
			$config['last_link']        = 'Last';
			$config['next_link']        = 'Next';
			$config['prev_link']        = 'Prev';
			$config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
			$config['full_tag_close']   = '</ul></nav></div>';
			$config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
			$config['num_tag_close']    = '</span></li>';
			$config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
			$config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
			$config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
			$config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
			$config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
			$config['prev_tagl_close']  = '</span>Next</li>';
			$config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
			$config['first_tagl_close'] = '</span></li>';
			$config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
			$config['last_tagl_close']  = '</span></li>';
			$this->pagination->initialize($config);		
			$a['data']					=	$this->crud_model->select_paging_where_like($this->tabel,$where,$config['per_page'],$from,"tahun","DESC",$like,$like_match);
			$a['kategori']				=	$this->crud_model->select_all_order("kategori","id_kategori","ASC");
			$a['status']				=	$this->crud_model->select_all_order("status","id_status","ASC");
			
			$this->bread[]	=	array(
				"active"	=>	FALSE,
				"icon"		=>	"icon-home home-icon",
				"link"		=>	site_url(),
				"label"		=>	"Dashboard",
				"divider"	=>	TRUE,
			);
			
			$this->bread[]	=	array(
				"active"	=>	TRUE,
				"icon"		=>	"",
				"link"		=>	"",
				"label"		=>	"Pembentukan",
				"divider"	=>	FALSE,
			);
			
			$a['bread']		=	$this->bread;
			$a['tabel']		=	$this->tabel;
			$a['key']		=	$this->key;
			$a['aksi']		=	"";
			$this->load->view("backend/main", $a);
	}
	
	// detail
	public function detail(){
		$id	=	$this->uri->segment(3);
		$detail				=	$this->crud_model->select_one($this->tabel,$this->key,$id);
		$status				=	$this->crud_model->select_all_where_order("man_status","id_kategori",$detail->kategori,"id_status","ASC");
		$riwayat			=	$this->crud_model->select_all_where_order("riwayat","id",$detail->id,"tanggal","DESC");
		$keterangan			=	$this->crud_model->select_all_order("keterangan","id_keterangan","ASC");
		$a['page']			=	$this->page;
		$a['title']			=	"Detail Pembentukan";
		$a['base']			=	$this->base;
		$a['detail']		=	$detail;
		$a['status']		=	$status;
		$a['keterangan']	=	$keterangan;
		$a['riwayat']		=	$riwayat;
		
		$this->bread[]	=	array(
			"active"	=>	FALSE,
			"icon"		=>	"icon-home home-icon",
			"link"		=>	site_url(),
			"label"		=>	"Dashboard",
			"divider"	=>	TRUE,
		);
		
		$this->bread[]	=	array(
			"active"	=>	FALSE,
			"icon"		=>	"",
			"link"		=>	site_url($this->base),
			"label"		=>	"Pembentukan",
			"divider"	=>	TRUE,
		);
		
		$this->bread[]	=	array(
			"active"	=>	TRUE,
			"icon"		=>	"",
			"link"		=>	"",
			"label"		=>	"Detail",
			"divider"	=>	FALSE,
		);
		
		$a['bread']		=	$this->bread;
		$a['aksi']		=	"detail";
		$this->load->view("backend/main", $a);
	}
	
	// Cek status
	public function cek_status(){
		$status	=	$this->input->post("id");
		if($status == "5" || $status == "12"){
			$a['status']		=	$status;
			$a['keterangan']	=	$this->crud_model->select_all_order("keterangan","id_keterangan","ASC");
			$this->load->view("backend/inc/form_status", $a);
		}
	}
	
	// paraf skpd
	public function paraf_skpd(){
		$id		=	$this->uri->segment(3);
		$data	=	array("status"=>"7");
		$update	=	$this->crud_model->update("draft",$data,"id",$id);
		if($update){
			$this->crud_model->insert("riwayat",array("id"=>$id,"status"=>"7"));
			$notifikasi		=	array(
				"status"	=>	1, "pesan"	=>	"Berhasil Diparaf"
			);
		}else{
			$notifikasi		=	array(
				"status"	=>	0, "pesan"	=>	"Gagal Diparaf"
			);
		}
		$this->session->set_flashdata("notifikasi",$notifikasi);
		redirect($this->base."/detail/".$id);
	}
	
	public function update(){
		$id			=	$this->input->post("id");
		$status		=	$this->input->post("status");
		$data		=	array("status"=>$status);
		$detail		=	$this->crud_model->select_one("draft","id",$id);
		$kategori	=	$this->crud_model->select_one("kategori","id_kategori",$detail->kategori);
		if($status == "7"){
			$notifikasi		=	array(
				"status"	=>	0, "pesan"	=>	"Hanya SKPD yang bisa menambah paraf"
			);
		}else{
			if($status == "5"){
				if(empty($_FILES['draft_fix']['name'])){
					$notifikasi		=	array(
						"status"	=>	0, "pesan"	=>	"Draft Belum Diupload"
					);
				}else{
					$data['tentang']	=	$this->input->post("tentang");
					$config['upload_path']     = './uploads/fix/';
					$config['allowed_types']   = 'docx|docs|doc|pdf';
					$config['max_size']       	= 2000;
					
					$kode_draft		= random('8');
					$nama 			= $this->session->userdata("user")["nama"];
					$file_name 		= $_FILES['draft_fix']['name'];
					$new_name		= 'FIX '.$kategori->nama_kategori.' '.$kode_draft;
					$config['file_name'] = $new_name;
					
					$this->load->library('upload', $config);
					if ( $this->upload->do_upload('draft_fix')){
						$data_file = $this->upload->data();
						$data['fix']	=	$data_file['file_name'];
						// $data['fix']	=	strtoupper($kategori->nama_kategori)." TENTANG ".strtoupper($this->input->post("tentang"));
					}
				}
			}
			if($status == "12"){
				if(empty($_FILES['pdf']['name'])){
					$notifikasi		=	array(
						"status"	=>	0, "pesan"	=>	"PDF Belum Diupload"
					);
				}else{
					$data['nomor']	=	$this->input->post("nomor");
					$data['tahun']	=	$this->input->post("tahun");
					$data['ket']	=	$this->input->post("keterangan");
					$config['upload_path']     = './uploads/pdf/';
					$config['allowed_types']   = 'pdf';
					$config['max_size']       	= 2000;
					
					$kode_draft		= random('8');
					$nama 			= $this->session->userdata("user")["nama"];
					$kategori 		= $kategori->nama_kategori;
					$file_name 		= $_FILES['pdf']['name'];
					$new_name		= $kategori.' NOMOR '.$this->input->post("nomor").' TAHUN '.$this->input->post("tahun");
					$config['file_name'] = $new_name;
					
					$this->load->library('upload', $config);
					if ( $this->upload->do_upload('pdf')){
						$data_file = $this->upload->data();
						$data['pdf']	=	$data_file['file_name'];
						// $data['pdf']	=	strtoupper($kategori->nama_kategori)." TENTANG ".strtoupper($detail->tentang);
					}
				}
			}
			$update	=	$this->crud_model->update("draft",$data,"id",$id);
			if($update){
				$this->crud_model->insert("riwayat",array("id"=>$id,"status"=>$status));
				$notifikasi		=	array(
					"status"	=>	1, "pesan"	=>	"Pembentukan Berhasil Diperbarui"
				);
			}else{
				$notifikasi		=	array(
					"status"	=>	0, "pesan"	=>	"Pembentukan Gagal Diperbarui"
				);
			}
		}
		$this->session->set_flashdata("notifikasi",$notifikasi);
		redirect($this->base."/detail/".$id);
	}
	
	public function tambah(){
		$a	=	array();			
		$a['page']		=	"/tambah_draft";
		$a['title']		=	"Tambah Draft";
		$a['base']		=	$this->base;
		$a['ket']		=	$this->ket;
		$a['kategori']				=	$this->crud_model->select_all_order("kategori","id_kategori","ASC");
		
		$this->bread[]	=	array(
			"active"	=>	FALSE,
			"icon"		=>	"icon-home home-icon",
			"link"		=>	site_url(),
			"label"		=>	"Dashboard",
			"divider"	=>	TRUE,
		);
		
		$this->bread[]	=	array(
			"active"	=>	TRUE,
			"icon"		=>	"",
			"link"		=>	site_url("Pembentukan"),
			"label"		=>	"Pembentukan",
			"divider"	=>	TRUE,
		);
		
		$this->bread[]	=	array(
			"active"	=>	TRUE,
			"icon"		=>	"",
			"link"		=>	"",
			"label"		=>	"Tambah",
			"divider"	=>	FALSE,
		);
		
		$a['bread']		=	$this->bread;
		$a['tabel']		=	$this->tabel;
		$a['key']		=	$this->key;
		$this->load->view("backend/main", $a);
	}
	
	// tambah aksi
	public function tambah_aksi(){
		$id				=	$this->crud_model->cek_id($this->tabel,$this->key);
		$kategori		=	$this->input->post("kategori");
		$tentang		=	$this->input->post("tentang");
		
		$data			=	array(
			"id"			=>	$id,
			"kategori"		=>	$kategori,
			"tentang"		=>	$tentang,
			"skpd"			=>	$this->session->userdata("user")["nama"],
			"tanggal_masuk"	=>	date("Y-m-d"),
			"status"		=>	"1"
		);
		
		$config['upload_path']     = './uploads/draft/';
		$config['allowed_types']   = '*';
		$config['max_size']       	= 10000;
		
		$kode_draft		= random('8');
		$nama 			= $this->session->userdata("user")["nama"];
		$kategori 		= ambil_nama_by_id("kategori","nama_kategori","id_kategori",$kategori);
		$tentang 		= $tentang;
		$tanggal_upload	= gmdate('Y/m/d', time()+60*60*8);
		$file_name 		= $_FILES['draft']['name'];
		$new_name		=	str_replace("/","-",$tanggal_upload).' DRAFT '.$kategori.' '.$kode_draft;
		$config['file_name'] = $new_name;
		
		$data_kategori	=	$this->crud_model->select_one("kategori","id_kategori",$kategori);
		
		$this->load->library('upload', $config);
		if ( $this->upload->do_upload('draft')){
			$data_file = $this->upload->data();
			$data['draft']	=	$data_file['file_name'];
		}
		
		$tambah		=	$this->crud_model->insert($this->tabel,$data);
		if($tambah){
			$this->crud_model->insert("riwayat",array("id"=>$id,"status"=>"1"));
			$notifikasi		=	array(
				"status"	=>	1, "pesan"	=>	$nama." Berhasil Ditambah"
			);
		}else{
			$notifikasi		=	array(
				"status"	=>	0, "pesan"	=>	$nama." Gagal Ditambah"
			);
		}
		$this->session->set_flashdata("notifikasi",$notifikasi);
		redirect($this->base);
	}
}