<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config = array(
	'testing'	=> array(				array(				'field' => 'username',				'label' => 'Username',				'rules' => 'required'		),		array(				'field' => 'password',				'label' => 'Password',				'rules' => 'required',				'errors' => array(						'required' => 'You must provide a %s.',				),		),		array(				'field' => 'passconf',				'label' => 'Password Confirmation',				'rules' => 'required|matches[password]'		),		array(				'field' => 'email',				'label' => 'Email',				'rules' => 'required'		)	), 

);


/* End of file form_validation.php */
/* Location: ./application/config/form_validation.php */
