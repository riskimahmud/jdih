<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'frontend';
$route['404_override'] = 'e404';
$route['translate_uri_dashes'] = false;

$route['login'] = 'login';
$route['login/do_login'] = 'login/do_login';
$route['login/logout'] = 'login/logout';

$route['admin/home'] = 'admin/profil';
$route['admin/profil'] = 'admin/reset_password';

$route['admin/reset_password'] = 'admin/reset_password';

$route['profil/tambah_aksi'] = 'm_berita/tambah_aksi';
$route['profil/perbarui_aksi'] = 'm_berita/perbarui_aksi';

$route['profil'] = 'm_berita/index/profil';
$route['profil/tambah_aksi'] = 'm_berita/tambah_aksi';
$route['profil/perbarui_aksi'] = 'm_berita/perbarui_aksi';

$route['berita'] = 'm_berita/index/berita';
$route['berita/tambah_aksi'] = 'm_berita/tambah_aksi';
$route['berita/perbarui_aksi'] = 'm_berita/perbarui_aksi';

$route['pengumuman'] = 'm_berita/index/pengumuman';
$route['pengumuman/tambah_aksi'] = 'm_berita/tambah_aksi';
$route['pengumuman/perbarui_aksi'] = 'm_berita/perbarui_aksi';

$route['produk/semua'] = 'frontend/produk';
$route['produk/peraturan-daerah'] = 'frontend/produkbyKategori/1';
$route['produk/peraturan-walikota'] = 'frontend/produkbyKategori/2';
$route['produk/peraturan-lainnya'] = 'frontend/produkbyKategori/0';

$route['produk/semua/(:any)'] = 'frontend/produk/$1';
$route['produk/peraturan-daerah/(:any)'] = 'frontend/produkbyKategori/1/$1';
$route['produk/peraturan-walikota/(:any)'] = 'frontend/produkbyKategori/2/$1';
$route['produk/peraturan-lainnya/(:any)'] = 'frontend/produkbyKategori/0/$1';

$route['filter'] = 'frontend/filter';
$route['produk/filter/(:any)'] = 'frontend/filter/$1';
$route['detail-produk/(:any)'] = 'frontend/detail_produk/$1';

$route['struktur'] = 'frontend/profil/struktur-organisasi';
$route['kontak'] = 'frontend/kontak';
$route['pengaduan'] = 'frontend/pengaduan';
$route['visi-misi'] = 'frontend/profil/visimisi';
$route['struktur'] = 'frontend/profil/struktur-organisasi';
$route['daftar-berita'] = 'frontend/berita';
$route['detail-berita/(:any)'] = 'frontend/detail_berita/$1';
$route['(.*)'] = "none";
