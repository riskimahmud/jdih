       <!-- Start Blog Page -->
       <div class="row">
       	<div class="col-md-8 col-sm-8">
       		<div class="blog-section">

       			<!-- Start Post -->
       			<div class="blog-post image-post">
       				<!-- Post Thumb -->
                    <?php foreach ($data as $d) {?>
       				<div class="post-head">
       					<div
       						style="background-image: url('<?php echo base_url("uploads/berita/" . $d->gambar); ?>');background-position: center;background-size: cover">
       						<a href="<?php echo base_url("uploads/berita/" . $d->gambar); ?>" data-lightbox="blog-img">
       							<img alt="" src="<?php echo base_url("uploads/berita/" . $d->gambar); ?>"style="opacity: 0">
       						</a>
       					</div>
       				</div>
       				<!-- Post Content -->
       				<div class="post-content">
       					<div class="post-type"><i class="fa fa-newspaper-o"></i></div>
       					<h1><a href="<?php echo site_url("detail-berita/" . $d->id); ?>"><?php echo $d->judul; ?></a></h1>
       					<ul class="post-meta">
       						<li>Oleh : Jdih</li>
       						<li><?php echo tgl_full($d->create_at); ?></li>
       					</ul>
       					<p><?php echo substr($d->isi, 0, 600); ?>...</p>
                     <a class="btn btn-primary" href="<?php echo site_url("detail-berita/" . $d->id); ?>">Selengkapnya<i class="fa fa-angle-right"></i></a>
       				</div>
					   <?php }?>
       				<!-- Pagination -->
       			</div>
       			<!-- End Post -->
       		</div>
       	</div>
       	<!--Sidebar-->
       	<div class="col-md-4 col-sm-4">

       		<div class="sidebar right-sidebar">

       			<!-- Search Widget -->
       			<div class="widget widget-search">
       				<form action="#" method="post">
       					<input type="search" name="key" placeholder="Enter Keywords..." />
       					<button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
       				</form>
       			</div>


       			<!-- Popular Posts widget -->
       			<div class="widget widget-popular-posts">
       				<h3 class="section-title">Terpopuler</h3>
       				<ul>
					   <?php foreach ($populer as $d) {?>
       					<li>
       						<div class="widget-thumb">
       							<a href="<?php echo site_url("detail-berita/" . $d->id); ?>">
       								<div
       									style="background-image: url('<?php echo base_url("uploads/berita/" . $d->gambar); ?>');background-position: center;background-size: cover">
       									<img src="<?php echo base_url("uploads/berita/" . $d->gambar); ?>"
       										alt="" style="opacity: 0"></div>
       							</a>
       						</div>
       						<div class="widget-content">
       							<h5><a href="<?php echo site_url("frontend/detail_berita/" . $d->id); ?>"><?=$d->judul?></a></h5>
       							<span><?php echo tgl_full($d->create_at); ?></span>
       						</div>
       						<div class="clearfix"></div>
       					</li>

						<?php }?>
       				</ul>
       			</div>
				   <div class="widget widget-popular-posts">
						<h3 class="section-title">INFO NASIONAL</h3>
					<div id="gpr-kominfo-widget-container"></div>
					</div>
       		</div>
       	</div>
       	<!--End sidebar-->
       </div>
