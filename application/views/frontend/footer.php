		<!-- Start Copyright Section -->
		<div class="row">
			<div class="col-md-12">
				<div class="footer-section">

					<div class="row">

						<div class="col-md-3 col-sm-6">
							<div class="footer-address">
								<h3>Alamat</h3>
								<address>
									<strong>Jl. Nani Wartabone No. 4 Kota Gorontalo<br>
									Kota Gorontalo - Gorontalo<br>
								</address>

								<address>
																		<strong>Nomor Telepon :</strong> 085340015541<br>
									<strong>E-mail :</strong> jdih@gorontalokota.go.id								</address>
							</div>
						</div>



						<div class="col-md-3 col-sm-6">
							<div class="footer-link">
								<h3>Link Terkait</h3>
								<ul>
									<li><a href="#"><i class="fa fa-chevron-circle-right"></i>GorontaloKota.Go.Id</a></li>
									<li><a href="#"><i class="fa fa-chevron-circle-right"></i>GorontaloProv.Go.Id</a></li>
									<li><a href="#"><i class="fa fa-chevron-circle-right"></i>RevolusiMental.Go.Id</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3 col-sm-6">
							<div class="footer-social">
								<h3>Get In Touch</h3>
								<ul>
									<li><a href="#"><i class="fa fa-facebook"></i></a></li>
									<li><a href="#"><i class="fa fa-twitter"></i></a></li>
									<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
									<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
									<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
								</ul>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
		<!-- End Footer Section -->

		<!-- Start Copyright Section -->
		<div class="row">
			<div class="col-md-12">
				<div class="copyright-section">
					<div class="row">
						<div class="col-md-6 col-sm-6">
							<div class="copy">&copy; Copyright 2021 Pemerintah Kota Gorontalo | All Rights Reserved</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End Copyright Section -->

	<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>

	<script src="<?php echo base_url(); ?>assets_front/js/jquery-2.1.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/modernizr.custom.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/lightbox.min.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/jquery.appear.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/jquery.fitvids.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/jquery.nicescroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/superfish.min.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/supersubs.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/styleswitcher.js"></script>
	<script src="<?php echo base_url(); ?>assets_front/js/script.js"></script>
	<script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>

</body>
</html>