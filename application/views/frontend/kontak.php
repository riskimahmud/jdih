 <div class="row">
        <div class="col-md-8">
            <div class="contact-section">
               <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d318.7346351195845!2d123.05935337259113!3d0.5318537647508025!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x32792adc2a675f77%3A0x8606d635fca3dcd9!2sKantor%20Walikota%20Gorontalo!5e0!3m2!1sid!2sid!4v1624598453028!5m2!1sid!2sid" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
            </div>

        </div>
        <div class="col-md-4">
            <div class="contact-section">

                <h3 class="section-title">Informasi</h3>

                <!-- Some Info -->
                <p>Bagian Hukum Sekretariat Daerah Kota Gorontalo</p>

                <!-- Info - Icons List -->
                <ul class="icons-list">
                    <li><i class="fa fa-globe">  </i> <strong>Alamat:</strong>Jl. Nani Wartabone No. 4 Kota Gorontalo</li>
                    <li><i class="fa fa-envelope-o"></i> <strong>Email:</strong> jdih@gorontalokota.go.id</li>
                    <li><i class="fa fa-mobile"></i> <strong>Nomor Telepon:</strong> 085340015541</li>
                </ul>


                <h3 class="section-title">Jam Pelayanan</h3>

                <!-- Info - List -->
                <ul class="list-unstyled">
                    <li><strong>Senin - Jumat</strong> - 09:00 - 17:00 </li>
                    <li><strong>Sabtu - Minggu</strong> Libur</li>
                </ul>

            </div>
        </div>
    </div>
