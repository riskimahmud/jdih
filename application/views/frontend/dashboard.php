		<!-- Start Main Slider Section -->
		<div class="row">
			<div class="col-md-12">
				<div class="main-slider hidden-sm hidden-xs" id="main-slider">

					<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner" role="listbox">

							<div class="item active">
								<img src="<?php echo base_url(); ?>assets_front/images/slider/slider11.png"
									class="img-responsive" alt="Slider images 1">
								<div class="carousel-caption">
								</div>
							</div>
							<div class="item">
								<img src="<?php echo base_url(); ?>assets_front/images/slider/slider11.png"
									class="img-responsive" alt="Slider images 1">
								<div class="carousel-caption">
								</div>
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
							<i class="fa fa-angle-left"></i>
							<span class="sr-only">Previous</span>
						</a>
						<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
							<i class="fa fa-angle-right" aria-hidden="true"></i>
							<span class="sr-only">Next</span>
						</a>
					</div>

				</div>
			</div>
		</div> <!-- End Main Slider Section -->



		<!-- Start Service Section -->
		<div id="service" class="services-section">

			<div class="row">
				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="feature-2">
						<i class="fa fa-bookmark"></i>
						<a href="<?=base_url()?>produk/peraturan-daerah">
							<h4>Peraturan Daerah</h4>
						</a>

						<p>Telusuri Peraturan Daerah kota Gorontalo.</p>
					</div>
				</div><!-- /.col-md-4 -->

				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="feature-2">
						<i class="fa fa-bookmark"></i>
						<a href="<?=base_url()?>produk/peraturan-walikota">
							<h4>Peraturan Walikota</h4>
						</a>
						<p>Telusuri Peraturan Walikota Gorontalo. </p>
					</div>
				</div><!-- /.col-md-4 -->

				<div class="col-md-4 col-sm-6 col-xs-12">
					<div class="feature-2">
						<i class="fa fa-bookmark"></i>
						<a href="<?php base_url()?>produk/peraturan-lainnya">
							<h4>Lainnya</h4>
						</a>
						<p>Telusuri Peraturan/Keputusan Lainnya</p>
					</div>
				</div><!-- /.col-md-4 -->

			</div><!-- /.row -->

		</div>
		<!-- End Services Section -->

		<div class="row">
			<div class="col-md-8 col-sm-8">
				<!-- Start Search -->
				<div class="home-about-us">
					<h3 class="section-title">Telusuri Produk Hukum Kota Gorontalo</h3>
					<form method="post" action="<?=base_url()?>produk/filter" autocomplete="off">
						<div class="row">
							<div class="col-sm-12">
								<select class="form-control" style="width: 100%" name="kategori">

									<option value="">::Pilih Jenis Produk Hukum::</option>
									<?php
foreach ($kategori as $k) {
    if ($k->id_kategori == "3") {
        continue;
    }
    ?>
									<option value="<?php echo $k->id_kategori; ?>"><?php echo $k->nama_kategori; ?></option>
									<?php }?>
								</select>
								</select>
							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="nomor" placeholder="Nomor"
									style="width: 100%"><br>

							</div>
							<div class="col-sm-6">
								<input type="text" class="form-control" name="tahun" placeholder="Tahun"
									style="width: 100%"><br>
							</div>
							<div class="col-sm-12">
								<textarea onfocus="clearContents(this);" style="width: 100%" name="tentang"
									placeholder="Judul/Tentang"></textarea>
							</div>
							<div class="col-sm-3">
								<button type="submit" name="filter" class="btn btn-primary"><i class="fa fa-search" aria-hidden="true"></i>
									Cari</button>
							</div>

						</div>
					</form>

				</div>


				<div class="home-about-us">
					<h3 class="section-title">Update Produk Hukum Terbaru</h3>
					<br>
					<?php
foreach ($phukum as $d): ?>

					<a href="<?php echo site_url("detail-produk/" . $d->id); ?>">
						<h4><?=ambil_nama_by_id("kategori", "nama_kategori", "id_kategori", $d->kategori);?>
							Nomor <?=$d->nomor . " Tahun " . $d->tahun?>

						</h4>
					</a>
					<p>Tentang <?=$d->tentang?></p>
					<hr>

					<?php endforeach;?>
					<a href="#service">Telusuri Lainnya</a>


				</div>

			</div>

			<div class="col-md-4 col-sm-4">
				<div class="sidebar right-sidebar">

					<div class="widget widget-popular-posts">
						<h3 class="section-title">BERITA TERKINI</h3>
						<ul>
						 <?php foreach ($berita as $d) {?>
							<li>
								<div class="widget-thumb">
									<a href="">
										<div
											style="background-image: url('<?php echo base_url("uploads/berita/" . $d->gambar); ?>');background-position: center;background-size: cover">
											<img src="<?php echo base_url("uploads/berita/" . $d->gambar); ?>" alt="" style="opacity: 0"></div>
									</a>
								</div>
								<div class="widget-content">
									<h5><a href="<?php echo site_url("detail-berita/" . $d->id); ?>"><?=$d->judul?></a></h5>
									<span><?php echo tgl_full($d->create_at); ?></span>
								</div>
								<div class="clearfix"></div>
							</li>
						<?php }?>
						</ul>
					</div>

					<div class="widget widget-popular-posts">
						<h3 class="section-title">INFO NASIONAL</h3>
					<div id="gpr-kominfo-widget-container"></div>
					</div>
				</div>
			</div>

		</div>
