<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>JDIH Kota Gorontalo</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Facebook Opengraph integration: https://developers.facebook.com/docs/sharing/opengraph -->
  <meta property="og:title" content="">
  <meta property="og:image" content="">
  <meta property="og:url" content="">
  <meta property="og:site_name" content="">
  <meta property="og:description" content="">

  <!-- Twitter Cards integration: https://dev.twitter.com/cards/  -->
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:title" content="">
  <meta name="twitter:description" content="">
  <meta name="twitter:image" content="">

  <!-- Favicon -->
  <link href="<?php echo base_url(); ?>assets_front/img/favicon.ico" rel="icon">

  <!-- Google Fonts -->
  <link href=https://fonts.googleapis.com/css?family=Raleway:400,500,700|Roboto:400,900" rel="stylesheet">
    <!-- Bootstrap CSS  -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/bootstrap.css" type="text/css">

	<!-- Font Awesome CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/font-awesome.min.css" type="text/css">

	<!-- Owl Carousel CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/owl.carousel.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/owl.theme.css" type="text/css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/owl.transitions.css" type="text/css">

	<!-- Light Box CSS -->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets_front/css/lightbox.css" type="text/css">

	<!-- Construction CSS Styles  -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/style.css">

	<!-- Default Color -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/colors/light-red.css">
	<!-- Colors CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/colors/light-red.css" title="light-red">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/colors/green.css" title="green">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/colors/blue.css" title="blue">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/colors/light-blue.css" title="light-blue">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/colors/yellow.css" title="yellow">

	<!-- Responsive CSS Style -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/responsive.css">

	<!-- Css3 Transitions Styles  -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets_front/css/animate.css">
</head>


	<!-- Start Loader -->
	<div id="loader">
		<div class="spinner">
			<div class="dot1"></div>
			<div class="dot2"></div>
		</div>
	</div>
	<!-- End Loader -->

		<div class="container">
		<!-- Start Header Section -->
		<div class="header-section">
        <div class="row">
            <div class="col-md-5 col-sm-5">
                <div class="logo-img">
                    <a href="#"><img src="<?php echo base_url(); ?>assets_front/images/logojdih.png" class="img-responsive" alt=""></a>
                </div>
            </div>
        </div>
    </div>		<!-- End Header Section -->