
		<!-- Start Navigation Section -->
		<div class="navigation">

        <div class="navbar navbar-default navbar-top">
            <div class="navbar-header">
                <!-- Stat Toggle Nav Link For Mobiles -->
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>

            </div>
            <div class="navbar-collapse collapse">

                <!-- Start Navigation List -->
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?=base_url()?>">BERANDA</a>
                    </li>
                    <li>
                        <a href="#">PROFIL</a>
                        <ul class="dropdown">
                            <li>
                                <a href="<?=base_url()?>visi-misi.html">Visi dan Misi</a>
                            </li>
                            <li>
                                <a href="<?=base_url()?>struktur.html">Struktur Organisasi</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="#">PRODUK HUKUM</a>
                        <ul class="dropdown">
                        <li>
                                <a href="<?=base_url()?>produk/semua.html">Semua</a>

                            </li>
                            <li>

                                <a href="<?=base_url()?>produk/peraturan-daerah.html">Peraturan Daerah</a>

                            </li>
                            <li>
                                <a href="<?=base_url()?>produk/peraturan-walikota.html" >Peraturan Walikota</a>

                            </li>


                        </ul>
                    </li>
                    <li>
                        <a href="<?=base_url()?>daftar-berita.html">BERITA</a>
                    </li>
                    <li>
                        <a href="#">KONTAK</a>
                        <ul class="dropdown">
                        <li>
                                <a href="<?=base_url()?>kontak.html">KONTAK</a>
                            </li>
                            <li>
                                <a href="<?=base_url()?>pengaduan.html">PENGADUAN</a>
                            </li>

                        </ul>
                    </li>

                </ul>
                <!-- End Navigation List -->
            </div>
        </div>

    </div>		<!-- End Navigation Section -->
