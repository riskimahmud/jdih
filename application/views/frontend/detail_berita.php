 <div class="row">
        <div class="col-md-8 col-sm-8">
            <div class="blog-section">

                <!-- Start Single Post Area -->
                <div class="blog-post gallery-post">

                    <!-- Start Single Post (Gallery Slider) -->
                    <div class="post-head">


                        <a title="This is an image title" href="<?php echo base_url("uploads/berita/" . $data->gambar); ?>" data-lightbox-gallery="gallery1">
						<div style="background-image: url('<?php echo base_url("uploads/berita/" . $data->gambar); ?>');background-position: center;background-size: cover">
                            <img alt="" src="<?php echo base_url("uploads/berita/" . $data->gambar); ?>" style="opacity: 0">
                        </div>
                        </a>



                        <!-- Start Single Post Content -->
                        <div class="post-content">
                            <div class="post-type"><i class="fa fa-picture-o"></i></div>
                            <h3><?=$data->judul?></h3>
                            <ul class="post-meta">
                                <li>Oleh : alexfgr</a></li>
                                <li><?php echo tgl_full($data->create_at); ?></li>
                                <li><i class="fa fa-eye" aria-hidden="true"></i>  <?php echo $data->hits; ?></li>
                            </ul>
                            <?=$data->isi?>
                            <div class="post-bottom clearfix">
                                <div class="post-share">
                                    <span>Bagikan Ke:</span>
                                    <a class="facebook" onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')" href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo site_url("detail-berita/" . $data->id); ?>&display=popup&ref=plugin&src=share_button"><i class="fa fa-facebook"></i></a>
                                    <a class="twitter" href="https://twitter.com/intent/tweet?text=<?php echo site_url("detail-berita/" . $data->id); ?>"><i class="fa fa-twitter"></i></a>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <div class="col-md-4 col-sm-4">

            <div class="sidebar right-sidebar">

                <!-- Search Widget -->
                <div class="widget widget-search">
                    <form action="#" method="post">
                        <input type="search" name="key" placeholder="Enter Keywords..." />
                        <button class="search-btn" type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>


               	<div class="widget widget-popular-posts">
       				<h3 class="section-title">Terpopuler</h3>
       				<ul>
					   <?php foreach ($populer as $d) {?>
       					<li>
       						<div class="widget-thumb">
       							<a href="<?php echo site_url("detail-berita/" . $d->id); ?>">
       								<div
       									style="background-image: url('<?php echo base_url("uploads/berita/" . $d->gambar); ?>');background-position: center;background-size: cover">
       									<img src="<?php echo base_url("uploads/berita/" . $d->gambar); ?>"
       										alt="" style="opacity: 0"></div>
       							</a>
       						</div>
       						<div class="widget-content">
       							<h5><a href="<?php echo site_url("detail-berita/" . $d->id); ?>"><?=$d->judul?></a></h5>
       							<span><?php echo tgl_full($d->create_at); ?></span>
       						</div>
       						<div class="clearfix"></div>
       					</li>

						<?php }?>
       				</ul>
       			</div>
				   <div class="widget widget-popular-posts">
						<h3 class="section-title">INFO NASIONAL</h3>
					<div id="gpr-kominfo-widget-container"></div>
					</div>




            </div>
        </div>
        <!--End sidebar-->
    </div>