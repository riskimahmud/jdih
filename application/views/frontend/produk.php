 <div class="row">
 	<div class="col-md-12">
 		<div class="welcome-text">
 			<h3 class="section-title"><?= $title ?></h3>

 			<form method="post" action="<?php echo base_url("filter") ?>">
 				<div class="row">
 					<?php

						if ($kat == "semua") { ?>
 						<div class="col-sm-12">
 							<select class="form-control" style="width: 100%" name="kategori">
 								<option value="">::Pilih Jenis Produk Hukum::</option>
 								<?php
									foreach ($kategori as $k) {
										if ($k->id_kategori == "3") {
											continue;
										}
									?>
 									<option value="<?php echo $k->id_kategori; ?>"><?php echo $k->nama_kategori; ?></option>
 								<?php } ?>
 							</select>
 						</div>
 					<?php } ?>
 					<?php
						if ($kat == 1) {
							echo "<input type='hidden' name='kategori' value='1'>";
						}

						if ($kat == 2) {
							echo "<input type='hidden' name='kategori' value='2'>";
						}

						?>
 					<div class="col-sm-6">
 						<input type="text" class="form-control" name="nomor" placeholder="Nomor" style="width: 100%"><br>

 					</div>
 					<div class="col-sm-6">
 						<input type="text" class="form-control" name="tahun" placeholder="Tahun" style="width: 100%"><br>
 					</div>
 					<div class="col-sm-12">
 						<textarea onfocus="clearContents(this);" style="width: 100%" name="tentang" placeholder="Judul/Tentang"></textarea>
 					</div>
 					<div class="col-sm-3">
 						<button type="submit" name="filter" value="filter" class="btn btn-warning"><i class="fa fa-search" aria-hidden="true"></i>
 							Cari</button>
 					</div>

 				</div>
 			</form>
 			<hr>
 			<table id="products" class="table table-hover" style="font-size:13px;">
 				<thead>
 					<tr>
 						<th>No.</th>
 						<th>Kategori</th>
 						<th>Nomor / Tahun</th>
 						<th>Tentang</th>
 						<th>Status</th>
 					</tr>
 				</thead>
 				<tbody>
 					<?php
						if (empty($data)) {
							echo '<tr><td colspan="5">Tidak Ditemukan.</td></tr>';
						} else {
							$no = $this->uri->segment('3') + 1;
							foreach ($data as $d) {
						?>
 							<tr onClick="top.location.href='<?php echo site_url("detail-produk/" . $d->id); ?>'" style="cursor:pointer;">
 								<td><?php echo $no; ?></td>
 								<td><?php echo ambil_nama_by_id("kategori", "nama_kategori", "id_kategori", $d->kategori); ?>
 								</td>
 								<td><?php echo $d->nomor . " / " . $d->tahun; ?></td>
 								<td><?php echo $d->tentang; ?></td>
 								<td><?php echo ambil_nama_by_id("keterangan", "nama_keterangan", "id_keterangan", $d->ket); ?>
 								</td>
 							</tr>
 					<?php $no++;
							}
						} ?>
 				</tbody>
 			</table>
 		</div>
 		<div class="pagination">
 			<?php
				echo $this->pagination->create_links();
				?>
 		</div>
 	</div>