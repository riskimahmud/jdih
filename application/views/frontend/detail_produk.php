<div class="row">
	<div class="col-md-12">
		<div class="welcome-text">
			<h3 class="section-title">DETAIL PRODUK HUKUM</h3>
			<div class="row">
				<div class="col-md-6">
					<table class="table">
						<tr>
							<td>Kategori</td>
							<td>:</td>
							<td><?php echo ambil_nama_by_id("kategori", "nama_kategori", "id_kategori", $data->kategori); ?></td>
						</tr>
						<tr>
							<td>Nomor</td>
							<td>:</td>
							<td><?php echo $data->nomor; ?></td>
						</tr>
						<tr>
							<td>Tahun</td>
							<td>:</td>
							<td><?php echo $data->tahun; ?></td>
						</tr>
						<tr>
							<td>Tentang</td>
							<td>:</td>
							<td><?php echo $data->tentang; ?></td>
						</tr>
						<tr>
							<td>Status</td>
							<td>:</td>
							<td><?php echo ambil_nama_by_id("keterangan", "nama_keterangan", "id_keterangan", $data->ket); ?></td>
						</tr>
						<tr>
							<td>Dilihat</td>
							<td>:</td>
							<td><?php echo $data->hits; ?></td>
						</tr>
						<tr>
							<td>Diunduh</td>
							<td>:</td>
							<td><?php echo $data->downloads; ?></td>
						</tr>
						<tr>
							<td colspan="3">
								<a href="https://klinikhukum.gorontalokota.go.id/frontend/download/<?= $data->id ?>" class="btn btn-primary btn-block" target="_blank">Download</a>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-md-6">
					<embed src="https://drive.google.com/viewerng/viewer?embedded=true&url=https://klinikhukum.gorontalokota.go.id/uploads/pdf/<?= $data->pdf ?>" width="500" height="500" />
				</div>
				<div class="a2a_kit a2a_kit_size_32 a2a_default_style">
					<a class="a2a_dd" href="https://www.addtoany.com/share"></a>
					<a class="a2a_button_facebook"></a>
					<a class="a2a_button_twitter"></a>
					<a class="a2a_button_email"></a>
					<a class="a2a_button_whatsapp"></a>
				</div>
				<script async src="https://static.addtoany.com/menu/page.js"></script>
				<!-- AddToAny END -->

			</div>
		</div>
		</section>