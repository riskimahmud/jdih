<?php
header_no_cache();
$this->load->view("backend/inc/header");
?>
	<div class="main-container container-fluid">
		<a class="menu-toggler" id="menu-toggler" href="#">
			<span class="menu-text"></span>
		</a>
    <!-- main sidebar -->
		<?php $this->load->view("backend/inc/sidebar");?>
	<!-- main sidebar end -->
	</div>
    <div class="main-content">
		<?php $this->load->view("backend/inc/head_html");?>
		<?php $this->load->view("backend" . $page);?>
		<?php $this->load->view("backend/inc/foot_html");?>
    </div>
<?php $this->load->view("backend/inc/footer");?>