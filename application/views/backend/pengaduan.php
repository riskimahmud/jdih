<div class="row-fluid">
<?php
	$aksi	=	get_value("a");
	$id		=	get_value("id");
	switch($aksi){
		case "detail":
			$row	=	ambil_data_by_id_row($tabel,$key,$id);
			$this->crud_model->update("aduan",array("status"=>"confirmed"),"id",$id);
?>
	<div class="span12">
		<div class="widget-box">
			<div class="widget-header">
				<h4><i class="icon-plus"></i> Detail <?php echo $label; ?></h4>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<div class="row-fluid">
						<div class="span6">
							<table class="table">
								<tr>
									<td>Tanggal Aduan</td>
									<td>:</td>
									<td><?php echo tgl_indonesia($row->tanggal_aduan); ?></td>
								</tr>
								<tr>
									<td>Pelapor</td>
									<td>:</td>
									<td><?php echo $row->nama; ?></td>
								</tr>
								<tr>
									<td>No. KTP</td>
									<td>:</td>
									<td><?php echo $row->no_ktp; ?></td>
								</tr>
								<tr>
									<td>No. Telp</td>
									<td>:</td>
									<td><?php echo $row->no_telp; ?></td>
								</tr>
								<tr>
									<td>Pekerjaan</td>
									<td>:</td>
									<td><?php echo $row->pekerjaan; ?></td>
								</tr>
								<tr>
									<td>Alamat</td>
									<td>:</td>
									<td><?php echo $row->alamat; ?></td>
								</tr>
							</table>
						</div>
						<div class="span6">
							<table class="table">
								<tr>
									<td>Terlapor</td>
									<td>:</td>
									<td><?php echo $row->terlapor; ?></td>
								</tr>
								<tr>
									<td>Isi Aduan</td>
									<td>:</td>
									<td><?php echo $row->isi_aduan; ?></td>
								</tr>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php
		break;
		
		default:
?>
	<div class="span12">
		<div class="widget-box">
			<div class="widget-header">
				<h4><i class="icon-list"></i> Daftar <?php echo $label; ?></h4>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<div class="row-fluid">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>Tanggal Aduan</th>
									<th>Pelapor</th>
									<th>NO. KTP</th>
									<th>Telp</th>
									<th>Alamat</th>
									<th>Terlapor</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no	=	1;
									foreach($data as $d){
										$bgcolor="";
										if($d->status == "new"){ $bgcolor = "red"; }
								?>
								<tr style="color:<?php echo $bgcolor; ?>">
									<td>
										<?php echo $no; ?>
									</td>
									<td><?php echo $d->tanggal_aduan; ?></td>
									<td><?php echo $d->nama; ?></td>
									<td><?php echo $d->no_ktp; ?></td>
									<td><?php echo $d->no_telp; ?></td>
									<td><?php echo $d->alamat; ?></td>
									<td><?php echo $d->terlapor; ?></td>
									<td>
										<a class="btn btn-inverse btn-mini" href="?a=detail&id=<?php echo $d->id; ?>">Detail</a>
									</td>
								</tr>
								<?php
									$no++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
		break;
	}
?>