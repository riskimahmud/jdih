<div class="sidebar" id="sidebar">
	<div class="sidebar-shortcuts" id="sidebar-shortcuts">
		<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
			<a href="<?php echo site_url("login/logout"); ?>" class="btn btn-small btn-danger">
				<i class="icon-signout"></i>
			</a>

			<a href="<?php echo site_url("admin/reset_password"); ?>" class="btn btn-small btn-info">
				<i class="icon-key"></i>
			</a>
			<a href="<?php echo site_url("admin/profil"); ?>" class="btn btn-small btn-warning">
				<i class="icon-user"></i>
			</a>
		</div>
	</div>
	<!--#sidebar-shortcuts-->

	<ul class="nav nav-list">
		<li style="margin-top:10px; margin-bottom:10px">
			<div style="padding:10px;">
				<span style="font-size:15px;" title="<?php echo get_userdata_user("nama"); ?>">
					<i class="icon-user orange home-icon"></i> <small title="<?php echo get_userdata_user("nama"); ?>"><?php echo substr(get_userdata_user("nama"), 0, 18); ?></small>
				</span>
				<br>
				<small class="text-muted" style="margin-left:20px;">
					<?php echo level_user(); ?>
				</small>
			</div>
		</li>
		<li>
			<a href="<?php echo site_url("admin/home"); ?>">
				<i class="icon-dashboard"></i>
				<span class="menu-text"> Dashboard </span>
			</a>
		</li>
		<?php
if (get_userdata_user("type") == "admin") {
    ?>
			<li style="padding:10px; font-size:15px;">
				Menu Admin
			</li>
			<li>
				<a href="<?php echo site_url("profil"); ?>">
					<i class="icon-building"></i>
					<span class="menu-text"> Profil </span>
				</a>
			</li>
			<li>
				<a href="<?php echo site_url("berita"); ?>">
					<i class="icon-list"></i>
					<span class="menu-text"> Berita </span>
				</a>
			</li>
			<li>
				<a href="<?php echo site_url("pengumuman"); ?>">
					<i class="icon-bullhorn"></i>
					<span class="menu-text"> Pengumuman </span>
				</a>
			</li>
		<?php }?>
	</ul>
	<!--/.nav-list-->

	<!--div class="sidebar-collapse" id="sidebar-collapse">
		<i class="icon-double-angle-left"></i>
	</div-->
</div>