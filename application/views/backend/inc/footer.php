<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-small btn-inverse">
	<i class="icon-double-angle-up icon-only bigger-110"></i>
</a>

<!--basic scripts-->
<!--script src="<?php //echo base_url();
?>assets/js/jquery-ui-1.10.3.custom.min.js"></script-->
<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>

<script type="text/javascript">
	window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
</script>

<script type="text/javascript">
	if ("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
</script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

<!--Datepicker-->
<script src="<?php echo base_url(); ?>assets/js/date-time/bootstrap-datepicker.min.js"></script>

<!--Colorbox-->
<script src="<?php echo base_url(); ?>assets/js/jquery.colorbox-min.js"></script>
<!--ace scripts-->

<!-- datatable -->
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.bootstrap.js"></script>

<script src="<?php echo base_url(); ?>assets/js/ace-elements.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/ace.min.js"></script>
<script type="text/javascript">
	function confirm_delete() {
		return confirm('are you sure?');
	}

	$(function() {
		var oTable1 = $('#datatable').dataTable();

		var colorbox_params = {
			reposition: true,
			scalePhotos: true,
			scrolling: false,
			previous: '<i class="icon-arrow-left"></i>',
			next: '<i class="icon-arrow-right"></i>',
			close: '&times;',
			current: '{current} of {total}',
			maxWidth: '100%',
			maxHeight: '100%',
			onOpen: function() {
				document.body.style.overflow = 'hidden';
			},
			onClosed: function() {
				document.body.style.overflow = 'auto';
			},
			onComplete: function() {
				$.colorbox.resize();
			}
		};

		$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
		$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>"); //let's add a custom loading icon
		$('.date-picker').datepicker().next().on(ace.click_event, function() {
			$(this).prev().focus();
		});

		/**$(window).on('resize.colorbox', function() {
			try {
				//this function has been changed in recent versions of colorbox, so it won't work
				$.fn.colorbox.load();//to redraw the current frame
			} catch(e){}
		});*/
	})
</script>

<!--inline scripts related to this page-->
</body>

</html>