<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8" />
	<title><?php echo $title; ?></title>

	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!--basic styles-->

	<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" />

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui-1.10.3.custom.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.gritter.css" />

	<!--colorbox-->
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/colorbox.css" />

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/font.css" />

	<!--ace styles-->

	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-responsive.min.css" />
	<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ace-skins.min.css" />

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body>
	<div class="navbar">
		<div class="navbar-inner">
			<div class="container-fluid">
				<a href="#" class="brand" style="margin-top:10px;">
					JDIH | <span class="orange">Kota Gorontalo</span>
					<small>
						<?php //echo $this->session->userdata("data_aplikasi")['nama_aplikasi']; 
						?>
					</small>
				</a>
				<!--/.brand-->

				<ul class="nav ace-nav pull-right">

					<li>
						<a data-toggle="dropdown" href="#" class="dropdown-toggle" style="height:200px;">
							<img class="nav-user-photo" src="<?php echo base_url("assets/img/user-eparty.png"); ?>" alt="No Poto" />

							<i class="icon-caret-down"></i>
						</a>

						<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer">
							<li>
								<a href="<?php echo site_url("admin/profil"); ?>">
									<i class="icon-user"></i>
									Profile
								</a>
							</li>

							<li class="divider"></li>

							<li>
								<a href="<?php echo site_url("login/logout"); ?>">
									<i class="icon-off"></i>
									Logout
								</a>
							</li>
						</ul>
					</li>
				</ul>
				<!--/.ace-nav-->
			</div>
			<!--/.container-fluid-->
		</div>
		<!--/.navbar-inner-->
	</div>
