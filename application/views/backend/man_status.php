<div class="row-fluid">
<?php
	$aksi	=	get_value("a");
	$id		=	get_value("id");
	switch($aksi){
		case "edit":
			$status	=	ambil_data("status","id_status","ASC");
			// $row	=	ambil_data_by_id_row($tabel,$key,$id);
?>
	<div class="span12">
		<div class="widget-box">
			<div class="widget-header">
				<h4><i class="icon-pencil"></i> Edit <?php echo $label; ?></h4>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<div class="row-fluid">
						<form class="form-horizontal" action="<?php echo site_url($base."/perbarui_aksi"); ?>" method="POST" autocomplete="off"/>
							<input type="hidden" class="span12" id="form-field-1" name="id" value="<?php echo $id; ?>"/>
							<div class="control-group">
								<label class="control-label" for="form-field-1">Kategori</label>

								<div class="controls">
									<input type="text" class="span12" name="nama" value="<?php echo ambil_nama_by_id("kategori","nama_kategori","id_kategori",$id); ?>" readonly />
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="form-field-1">Status</label>

								<div class="controls">
									<?php
										foreach($status as $s){
											$where		=	array("id_kategori"=>$id,"id_status"=>$s->id_status);
											$checked	=	cek_data_where_array("man_status",$where);
									?>
									<label>
										<input name="status[]" class="ace-checkbox-2" type="checkbox" value="<?php echo $s->id_status; ?>" <?php if(!$checked){ echo "checked"; } ?>/>
										<span class="lbl"> <?php echo $s->nama_status; ?></span>
									</label>
									<?php
										}
									?>
								</div>
							</div>
							<div class="form-actions">
								<button type="submit" class="btn btn-small btn-inverse">
									Simpan
									<i class="icon-arrow-right icon-on-right bigger-110"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php	
		break;
		
		default:
?>
	<div class="span12">
		<div class="widget-box">
			<div class="widget-header">
				<h4><i class="icon-list"></i> Daftar <?php echo $label; ?></h4>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<div class="row-fluid">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kategori</th>
									<th>Status</th>
									<th>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no	=	1;
									foreach($data as $d){
										$status	=	ambil_data_by_id_order("man_status","id_kategori",$d->id_kategori,"id_status","ASC");
								?>
								<tr>
									<td>
										<?php echo $no; ?>
									</td>
									<td><?php echo $d->nama_kategori; ?></td>
									<td>
										<?php 
											if(empty($status)){ echo "-"; }
											else{ 
												$si	=	1;
												foreach($status as $s){
													if($si > 1){ echo " - "; }
													echo '<span class="label label-primary">'.ambil_nama_by_id("status","nama_status","id_status",$s->id_status).'</span>';
												$si++;
												}
											} 
										?>
									</td>
									<td>
										<a class="btn btn-inverse btn-mini" href="?a=edit&id=<?php echo $d->id_kategori; ?>">Edit</a>
									</td>
								</tr>
								<?php
									$no++;
									}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
		break;
	}
?>