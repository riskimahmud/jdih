<div class="row-fluid">
<?php
	switch($aksi){
		case "detail":
?>
	<div class="span6">
		<div class="widget-box">
			<div class="widget-header">
				<h4>
					<i class="icon-list"></i> 
					Data Pembentukan
				</h4>
				<div class="widget-toolbar">
				</div>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<form action="<?php echo site_url($base."/update"); ?>" method="POST" class="form-horizontal" enctype="multipart/form-data" autocomplete="off">
					<input type="hidden" name="id" value="<?php echo $detail->id; ?>">
					<table class="table table-hover">
						<tr>
							<td>Pengusul</td>
							<td>:</td>
							<td><?php echo $detail->skpd; ?></td>
						</tr>
						<tr>
							<td>Tanggal Masuk</td>
							<td>:</td>
							<td><?php echo tgl_indonesia($detail->tanggal_masuk); ?></td>
						</tr>
						<tr>
							<td>Kategori</td>
							<td>:</td>
							<td><?php echo ambil_nama_by_id("kategori","nama_kategori","id_kategori",$detail->kategori); ?></td>
						</tr>
						<?php if($detail->status == "12"){ ?>
						<tr>
							<td>Nomor</td>
							<td>:</td>
							<td><?php echo $detail->nomor; ?></td>
						</tr>
						<tr>
							<td>Tahun</td>
							<td>:</td>
							<td><?php echo $detail->tahun; ?></td>
						</tr>
						<?php } ?>
						<tr>
							<td>Tentang</td>
							<td>:</td>
							<td><?php echo $detail->tentang; ?></td>
						</tr>
						<?php if($detail->status == "12"){ ?>
						<tr>
							<td>Status</td>
							<td>:</td>
							<td>
								<?php 
									echo ambil_nama_by_id("status","nama_status","id_status",$detail->status); 
								?>
							</td>
						</tr>
						<tr>
							<td>Keterangan</td>
							<td>:</td>
							<td>
								<?php 
									echo ambil_nama_by_id("keterangan","nama_keterangan","id_keterangan",$detail->ket); 
								?>
							</td>
						</tr>
						<?php 
							}else{ 
								if($this->session->userdata("user")['type'] == "skpd"){
						?>
						<tr>
							<td>Status</td>
							<td>:</td>
							<td>
								<?php 
									echo ambil_nama_by_id("status","nama_status","id_status",$detail->status); 
								?>
							</td>
						</tr>
						<tr>
							<td colspan="3">
							<?php if($detail->status == "6"){ ?>
							<a class="btn btn-primary btn-block" href="<?php echo site_url($base."/paraf_skpd/".$detail->id); ?>" onclick="return confirm('Apakah anda yakin?')">
								Paraf SKPD
							</a>
							<?php } ?>
							</td>
						</tr>
						<?php
								}else{
						?>
						<tr>
							<td>Status</td>
							<td>:</td>
							<td>
								<select class="span12" name="status" id="status">
								<?php 
									foreach($status as $s){
								?>
								<option value="<?php echo $s->id_status; ?>" <?php if($s->id_status == $detail->status){ echo 'selected="selected"'; } ?>>
									<?php echo ambil_nama_by_id("status","nama_status","id_status",$s->id_status); ?>
								</option>
								<?php
									}
								?>
								</select>
							</td>
						</tr>
						<?php 
								}
							} 
						?>
					</table>
						<div id="box-status">
						</div>
						<?php if($detail->status < 12 && $this->session->userdata("user")['type'] == "admin"){ ?>
						<button type="submit" class="btn btn-block btn-primary">Simpan</button>
						<?php } ?>
					</form>
				</div>
			</div>
		</div>
		
		<div class="widget-box">
			<div class="widget-header">
				<h4>
					<i class="icon-history"></i> 
					Riwayat
				</h4>
				<div class="widget-toolbar">
				</div>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<table class="table table-hover">
						<tr>
							<td>Tanggal</td>
							<td>Status</td>
						</tr>
						<?php 
							if(empty($riwayat)){ echo '<tr><td colspan="2">Belum ada data</td></tr>'; }else{ 
								foreach($riwayat as $r){
						?>
						<tr>
							<td><?php echo tgl_indonesia($r->tanggal) ?></td>
							<td><?php echo ambil_nama_by_id("status","nama_status","id_status",$r->status); ?></td>
						</tr>
						<?php 
								}
							} 
						?>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="span6">
		<div class="widget-box">
			<div class="widget-header">
				<?php 
					if($detail->status < 5){ $status = "File Draft"; $direktori = "draft"; $field = "draft";}  
					elseif($detail->status < 12){ $status = "File Draft Fix"; $direktori = "fix"; $field = "fix";} 
					elseif($detail->status == 12){ $status = "File PDF"; $direktori = "pdf"; $field = "pdf";} 
				?>
				<h4><i class="icon-file"></i> <?php echo $status; ?></h4>
				<div class="widget-toolbar">
					<a href="<?php echo base_url("uploads/".$direktori."/".$detail->$field); ?>" target="_blank">
						Download
					</a>
				</div>
			</div>
			<div class="widget-body">
				<div class="widget-main">
					<!--iframe src="<?php //echo base_url("uploads/".$direktori."/".$detail->$field); ?>" scrolling="auto" width="900px" height="500px" frameborder="0"></iframe-->
					<!--embed src="<?php //echo base_url("uploads/".$direktori."/".$detail->$field); ?>" width="900px" height="500px" frameborder="0"-->
					<embed
					  src="https://drive.google.com/viewerng/viewer?embedded=true&url=<?php echo base_url("uploads/".$direktori."/".$detail->$field); ?>"
					  width="500"
					  height="500"
					/>
				</div>
			</div>
		</div>
	</div>
<?php		
		break;
		
		default:
?>
	<div class="span12">
		<div class="widget-box">
			<div class="widget-header">
				<h4><i class="icon-list"></i> DAFTAR PEMBENTUKAN PRODUK HUKUM</h4>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<form class="form-inline span12" name="submit" action="<?php echo site_url($base."/filter"); ?>" method="POST" autocomplete="off"/>
						<select class="span2" name="kategori">
							<option value="0">- Pilih Kategori -</option>
							<?php foreach($kategori as $k){ ?>
							<option value="<?php echo $k->id_kategori; ?>"><?php echo $k->nama_kategori; ?></option>
							<?php } ?>
						</select>
						<select class="span2" name="status">
							<option value="0">- Pilih Status -</option>
							<?php foreach($status as $s){ ?>
							<option value="<?php echo $s->id_status; ?>"><?php echo $s->nama_status; ?></option>
							<?php } ?>
						</select>
						<input type="text" class="span1" placeholder="No." name="nomor" />
						<input type="text" class="span2" placeholder="Tahun" name="tahun" />
						<input type="text" class="span5" placeholder="Tentang" name="tentang" />

						<button type="submit" class="btn btn-info btn-small">
							<i class="icon-search bigger-110"></i>
							Cari
						</button>
					</form>
					<hr>
					<div class="row-fluid table-responsive">
						<table class="table table-hover">
							<thead>
								<tr>
									<th>No.</th>
									<th>Kategori</th>
									<th>Nomor</th>
									<th>Tahun</th>
									<th>Tentang / Status</th>
									<th width="5%">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
									if(empty($data)){
										echo '<tr><td colspan="6"><div class="alert alert-info">Belum ada data.</div></td></tr>';
									}else{
									// $no	=	1;
									$no = $this->uri->segment('3') + 1;
									foreach($data as $d){
										$kategori	=	ambil_nama_by_id("kategori","nama_kategori","id_kategori",$d->kategori);
										$status		=	ambil_nama_by_id("status","nama_status","id_status",$d->status);
										$warna		=	ambil_nama_by_id("status","label","id_status",$d->status);
								?>
								<tr>
									<td>
										<?php echo $no; ?>
									</td>
									<td>
										<?php echo $kategori; ?>
									</td>
									<td>
										<?php echo $d->nomor; ?>
									</td>
									<td>
										<?php echo $d->tahun; ?>
									</td>
									<td>
										<?php echo $d->tentang; ?>
										<br>
										<span class="label label-<?php echo $warna; ?>">
											<?php echo $status; ?>
										</span>
									</td>
									<td>
										<div class="btn-group">
											<a class="btn btn-inverse btn-mini" href="<?php echo site_url("pembentukan/detail/".$d->id); ?>">Detail</a>
										</div>
									</td>
								</tr>
								<?php
									$no++;
										}
									}
								?>
							</tbody>
						</table>
						<div class="pagination">
						<?php 
							echo $this->pagination->create_links();
						?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
		break;
	}
?>