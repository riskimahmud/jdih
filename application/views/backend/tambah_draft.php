<div class="row-fluid">
	<div class="span12">
		<div class="widget-box">
			<div class="widget-header">
				<h4><i class="icon-plus"></i> Tambah Draft</h4>
			</div>

			<div class="widget-body">
				<div class="widget-main">
					<div class="row-fluid">
						<form class="form-horizontal" action="<?php echo site_url($base."/tambah_aksi"); ?>" method="POST" autocomplete="off" enctype="multipart/form-data"/>
							<div class="control-group">
								<label class="control-label" for="form-field-1">Jenis Produk Hukum</label>

								<div class="controls">
									<select class="span12" name="kategori">
										<?php foreach($kategori as $k){ ?>
										<option value="<?php echo $k->id_kategori; ?>"><?php echo $k->nama_kategori; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="form-field-1">Tentang</label>

								<div class="controls">
									<input type="text" class="span12" id="form-field-1" name="tentang" placeholder="Tentang" required/>
								</div>
							</div>
							<div class="control-group">
								<label class="control-label" for="form-field-1">Draft</label>

								<div class="controls">
									<input type="file" class="span12" id="form-field-1" name="draft" required/>
								</div>
							</div>
							<div class="form-actions">
								<button type="submit" class="btn btn-small btn-inverse">
									Kirim
									<i class="icon-arrow-right icon-on-right bigger-110"></i>
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>